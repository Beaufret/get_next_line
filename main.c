/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbeaufre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/17 07:40:01 by rbeaufre          #+#    #+#             */
/*   Updated: 2019/07/09 17:13:56 by rbeaufre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line.h"
#include <stdio.h>

int			main(int ac, char **av)
{
	char	*res;
	int		fd;
	int		res_gnl;
	//int		i;

	//i = 0;
	res = NULL;
	(void)ac;
	fd = open(av[1], O_RDONLY);
	while ((res_gnl = get_next_line(fd, &res))) 
	{
		//printf("fd:%d, I vaut %i\n", fd, i);
		//i++;
		printf("res:%d line|%s|\n", res_gnl, res);
		free(res);
		res = NULL;
	}	
	res_gnl = get_next_line(fd, &res);
	free(res);
	res = NULL;
	printf("res:%d line|%s|\n", res_gnl, res);
	return (0);
}
